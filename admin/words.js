// Dictionary (systemDictionary is global variable from adapter-settings.js)
systemDictionary = {
	"mystrom_einstellungen": 	{"de": "myStrom Adapter", "en": "myStrom adapter"},
	"Lampe": {"de":"Lampe","en":"Light"},
	"Steckdose": {"de": "Steckdose", "en": "plug"},
	"url":                    {"en": "address (url)", "de": "Adresse (URL)"},
	"name":                    {"en": "name", "de": "Bezeichnung"},
	"on save adapter restarts with new config immediately": {
		"de": "Beim Speichern von Einstellungen der Adapter wird sofort neu gestartet.",
	}
};
